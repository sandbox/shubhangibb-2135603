Description 
-----------

Book tree outline module is developed to provide click and expand 
feature for book menus.This module is integrated with JQuery Menu 
module for click and expands functionality using different Jquery 
menu API's.
User can set the display of book block and expand depth level of 
tree from block configuration page.This is helpful for set default 
expand level of book tree.
Note: On any book detail page, block will display only tree belong 
to that rendered book page.
On other pages this block will display all the books in the system.


Installation
------------

Just download this module and put the modules files in modules folder
and enable it.You will find the block at block listing page.
Then you can configure that block wherever you want.

How to use
----------

Once module is enabled, you can use it to create a navigation 
book block. If you haven't yet created any books, you will 
need to do so first.
See http://drupal.org/handbook/modules/book 
for more information about books.

The administration page for book tree outline block is found at
(admin/structure/block/manage/book_tree_outline/navigation/configure).


Permissions
-----------

There is no specific permission for bookblock. It is available to
administrators with the "administer site configuration" permission.
